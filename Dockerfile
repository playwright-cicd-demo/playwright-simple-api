FROM node:10-alpine
WORKDIR /usr/dist/app
COPY package.json package-lock.json ./
RUN npm ci
COPY index.js .
EXPOSE 8080
CMD ["node", "."]