var express = require("express");
var bodyParser = require("body-parser");
var cors = require('cors');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());


app.get("/greeting/:name", function(req, res) {
    res.send("Hello, " + req.params.name);
});

var port = 8080;
app.listen(port);
console.log("Express running on port " + port);